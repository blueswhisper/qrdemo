//
//  ScannerViewController.h
//  QRDemo
//
//  Created by Yoyo on 14-4-9.
//  Copyright (c) 2014年 Yoyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"


@interface ScannerViewController : UIViewController<ZBarReaderDelegate,UINavigationControllerDelegate,
                                    UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *scanImage;
@property (weak, nonatomic) IBOutlet UILabel *resultText;
- (IBAction)scan:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)selectPicture:(id)sender;
@end
