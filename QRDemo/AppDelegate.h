//
//  AppDelegate.h
//  QRDemo
//
//  Created by Yoyo on 14-4-8.
//  Copyright (c) 2014年 Yoyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
