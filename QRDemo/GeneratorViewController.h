//
//  GeneratorViewController.h
//  QRDemo
//
//  Created by Yoyo on 14-4-9.
//  Copyright (c) 2014年 Yoyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QREncoder.h"

@interface GeneratorViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIImageView *genImage;
- (IBAction)genQR:(id)sender;
@end
