//
//  ScannerViewController.m
//  QRDemo
//
//  Created by Yoyo on 14-4-9.
//  Copyright (c) 2014年 Yoyo. All rights reserved.
//

#import "ScannerViewController.h"

@interface ScannerViewController ()

@end

@implementation ScannerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)scan:(id)sender {
    ZBarReaderController *reader = [ZBarReaderController new];
    reader.readerDelegate = self;
    ZBarImageScanner *scanner = reader.scanner;
    
    [scanner setSymbology: ZBAR_QRCODE
                   config: ZBAR_CFG_MAX_LEN
                       to: 0];
    
    CGImageRef imgCG = self.scanImage.image.CGImage;
    
    id<NSFastEnumeration> results = [reader scanImage:imgCG];
    ZBarSymbol *symbol = nil;
    for(ZBarSymbol *symbolF in results){
        symbol=symbolF;
        break;
    }
    
    if([symbol.data length]>0){
        self.resultText.text=symbol.data;
    }
    else
    {
        self.resultText.text=@"识别失败";
    }
}

- (IBAction)takePhoto:(id)sender {
    ZBarReaderViewController * reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    ZBarImageScanner * scanner = reader.scanner;
    [scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_MAX_LEN to:0];
    reader.showsZBarControls = YES;
    
    [self presentViewController:reader animated:YES completion:nil];

}

- (IBAction)selectPicture:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.scanImage setImage:selImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
